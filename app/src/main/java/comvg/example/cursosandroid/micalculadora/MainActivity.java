package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1;
    private EditText txtNum2 ;
    private EditText txtResult;
    private Button btnSumar;
    private  Button  btnRestar;
    private  Button  btnMult;
    private  Button  btnDiv;
    private  Button  btnLimpiar;
    private  Button  btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();
    }

    public void initComponents(){

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (EditText) findViewById(R.id.txtResult);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        setEventos();
    }

    public void setEventos(){

        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){

    }
}
